import os
import yaml
from datetime import date
import json
import subprocess
from common import get_env_value

dir_path = os.path.dirname(os.path.realpath(__file__))
query_link = "https://" + get_env_value('company_name') + ".visualstudio.com/" + get_env_value('team_name') + "/_queries?tempQueryId=" + get_env_value('queryID')
with open(os.path.join(dir_path, 'users.yaml'), 'r') as stream:
    users_data = yaml.load(stream)
users = users_data['users']
pmteam = users_data['pmteam']


slack_webhook_url = 'https://hooks.slack.com/services/' + get_env_value('slack_webhook')
vsts_pat = get_env_value('vsts_pat')
url = "https://" + vsts_pat + "@"+ get_env_value('company_name') +".visualstudio.com/" + get_env_value('team_name') + "/_apis/wit/wiql?api-version=5.0"
message = []
headers = "Content-Type: application/json"

for user in users:
    data = {
        "query": "SELECT [System.Id] FROM WorkItems WHERE [System.AreaPath] = '" + get_env_value('team_name') + "\\" + get_env_value('sub_team_name') + "' AND [System.ChangedDate] = @Today AND [System.ChangedBy] EVER '" + user + "'"
    }
    output = subprocess.Popen(['curl', '-k', '--header', headers, '--request', 'POST', '--data', json.dumps(data), url], stdout=subprocess.PIPE)
    (out, err) = output.communicate()
    response_json = json.loads(out.decode())
    work_item_list = response_json['workItems']
    id = []
    for work_item in work_item_list:
        id.append(str(work_item['id']))
    message.append("- " + user + " : " + ','.join(id))

today = date.today()
output = '\n'.join(message)

with open(os.path.join(dir_path, 'ado_tasks_updated_' + today.strftime("%d_%m_%Y")), 'w') as outfile:
    outfile.write(output)

pmteam_str = ' '.join(pmteam)

data = '{"text":"Hello ' + pmteam_str + ' ,\nThe following are the ' \
       'changes made today .\nPlease refer to ' + query_link + \
       ' for further details.\n' \
       '*ADO tasks modified on ' + today.strftime("%d_%m_%Y") + '* :\n ' \
       '```\n' + output + \
       '```"}'

output = subprocess.Popen(['curl', '-k', '--header', headers, '--request', 'POST', '--data', data, slack_webhook_url], stdout=subprocess.PIPE)
(out, err) = output.communicate()
