Updates to the Project management which item each members of the team worked on today. Integrates with Azure DevOps scrum board APIs and posts daily status on Slack channel.

Runs every evening through gitlab ci job